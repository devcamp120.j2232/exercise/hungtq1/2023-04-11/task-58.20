package com.devcamp.task58_20.drinklist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DrinklistApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrinklistApplication.class, args);
	}

}
