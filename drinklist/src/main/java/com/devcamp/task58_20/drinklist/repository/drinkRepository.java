package com.devcamp.task58_20.drinklist.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task58_20.drinklist.model.drinklist;

public interface drinkRepository extends JpaRepository<drinklist, Long>{
    
}
