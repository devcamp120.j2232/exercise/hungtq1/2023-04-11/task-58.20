package com.devcamp.task58_20.drinklist.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.devcamp.task58_20.drinklist.model.drinklist;
import com.devcamp.task58_20.drinklist.repository.drinkRepository;

@RestController("/")
@CrossOrigin
@RequestMapping
public class drinkController {
    @Autowired
    drinkRepository drinkRespo;
   

    @GetMapping("/drinks")
    public ResponseEntity<List<drinklist>> getAllDrinks(){
        try {
            List<drinklist> listCustomer = new ArrayList<drinklist>();
            
            drinkRespo.findAll().forEach(listCustomer::add);
            return new ResponseEntity<>(listCustomer, HttpStatus.OK);


        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
